# Twig Inline CSS
Adds the [inline_css](https://twig.symfony.com/doc/2.x/filters/inline_css.html) Twig filter to Drupal.
